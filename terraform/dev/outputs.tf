output "external_ip_address_gitlab" {
  value = module.gitlab.external_ip_address_gitlab
}
output "internal_ip_address_gitlab" {
  value = module.gitlab.internal_ip_address_gitlab
}
output "hostname" {
  value = module.gitlab.hostname
}
