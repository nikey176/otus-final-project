terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.73.0"
    }
  }
}

provider "yandex" {
  service_account_key_file   = var.service_account_key_file
  cloud_id                   = var.cloud_id
  folder_id                  = var.folder_id
  zone                       = var.zone
}
module "gitlab" {
  source                     = "../modules/gitlab"
  public_key_path            = var.public_key_path
  image_id                   = var.image_id
  subnet_id                  = var.subnet_id
  environment                = var.environment
  private_key_path           = var.private_key_path
  external_ip_address_gitlab = module.gitlab.external_ip_address_gitlab
  internal_ip_address_gitlab = module.gitlab.internal_ip_address_gitlab
  scale                      = var.scale
  boot_disk_size             = var.boot_disk_size
}
