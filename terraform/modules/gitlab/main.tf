terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.73.0"
    }
  }
}

resource "yandex_compute_instance" "gitlab-infra" {
  name  = "gitlab-${var.environment}-${count.index + 1}"
  count = var.scale
  labels = {
    tags = "gitlab"
  }

  resources {
    cores         = 2
    memory        = 8
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.boot_disk_size
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.public_key_path)}"
  }

  scheduling_policy {
    preemptible = true
  }

  allow_stopping_for_update = true

  # connection {
  #   type  = "ssh"
  #   host  = self.network_interface.0.nat_ip_address
  #   user  = "ubuntu"
  #   agent = false
  #   # путь до приватного ключа
  #   private_key = file(var.private_key_path)
  # }

  # provisioner "local-exec" {
  #   command     = "ansible-playbook playbooks/gitlab.yml"
  #   working_dir = "../../ansible"
  # }

}
