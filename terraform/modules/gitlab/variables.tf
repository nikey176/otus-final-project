variable "public_key_path" {
  # Описание переменной
  description = "Path to the public key used for ssh access"
}
#variable "docker_disk_image" {
#  description = "Disk image with docker"
#  default     = "docker-base"
#}
variable "image_id" {
  description = "Disk image with docker"
  default     = "docker-base"
}
variable "subnet_id" {
  description = "Subnets for modules"
}
variable "environment" {
  description = "Environment"
}
variable "private_key_path" {
  description = "Path to the private key used for ssh access"
}
variable "external_ip_address_gitlab" {
  description = "Gitlab external address"
}
variable "internal_ip_address_gitlab" {
  description = "Gitlab internal address"
}
variable "scale" {
  description = "Number of instances"
}
variable "boot_disk_size" {
  description = "Size of boot disk"
}
