output "external_ip_address_gitlab" {
  value = yandex_compute_instance.gitlab-infra[*].network_interface.0.nat_ip_address
}
output "internal_ip_address_gitlab" {
  value = yandex_compute_instance.gitlab-infra[*].network_interface.0.ip_address
}
output "hostname" {
  value = yandex_compute_instance.gitlab-infra[*].name
}
