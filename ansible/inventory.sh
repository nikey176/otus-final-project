#!/bin/bash
echo 'Собираем информацию...'

ANSIBLE_DIR=.
TERRAFORM_DIR=../terraform

HOST_NAME=`terraform -chdir=../terraform/dev/ show -json | jq '.values.outputs.hostname.value'`
HOST_ADDRESS=`terraform -chdir=../terraform/dev/ show -json | jq '.values.outputs.external_ip_address_gitlab.value'`

environment(){
cd $ANSIBLE_DIR
echo -e "[docker-hosts]"
for (( i = 0; i < `terraform -chdir=../terraform/dev/ show -json | jq '.values.outputs.external_ip_address_gitlab.value | length'`; i++ ))
do
echo -e `terraform -chdir=../terraform/dev/ show -json | jq -r --arg i $i ".values.outputs.hostname.value[$i]"` \
ansible_ssh_host=\
`terraform -chdir=../terraform/dev/ show -json | jq -r --arg i $i ".values.outputs.external_ip_address_gitlab.value[$i]"`
done
} > inventory
environment