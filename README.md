# Otus Final Project

#### Версии используемого ПО
- Packer 1.8.3
- Ansible 2.13.1
- Python 3.8.10

#### Версии используемых образов
- mongo:3.7
- rabbitmq:3.10.7
- prom/prometheus:v2.37.0
- prom/node-exporter:v1.3.1
- prom/alertmanager:v0.24.0
- gitlab/gitlab-ce latest
***
## Подготовка инфраструктуры
1. При помощи Packer создан образ диска с Docker, который будет служить основой для разворачиваемых ВМ  
2. В качестве провижинера для Packer использован плейбук Ansible  
```
ansible/playbooks/docker.yml
```
3. Через инструменты Yandex Cloud получены и сохранены в файл packer/variables.json следующие переменные:  
    - ключ служебного аккаунта в Yandex Cloud  
    - идентификатор папки в облаке  
    - идентификатор подсети  
    - семейство базового образа  
4. Для создания образа выполнить команды:  
```
$ cd packer
$ packer build -var-file=variables.json ./docker.json
```
5. Инструментом для развертывания инфраструктуры под Gitlab и Prometheus, а также для выполнения тестов приложения послужил Terraform
    - В директории terraform/dev настроена конфигурация с подключенным модулем gitlab
    - Настроены выходные переменные для получения адресов создаваемых хостов
6. Для создания виртуальных машин выполнить команды:
```
$ cd /terraform/dev
$ terraform apply --auto-approve
```
7. Для дальнейшей установки Gitlab и Prometheus использованы плейбуки Ansible
8. Для получения inventory-файла настроен скрипт
```
ansible/inventory.sh
```
9. Для установки Gitlab выполнить команды:
```
$ cd /ansible
$ ansible-playbook playbooks/gitlab.yml
```
## Подготовка Docker-образов приложения Crawler
1. Для бота и веб-интерфейса созданы отдельные репозитории
	https://gitlab.com/nikey176/search-engine-crawler
	https://gitlab.com/nikey176/search-engine-ui
2. В корне каждого репозитория создан файл Dockerfile
3. Для сборки образа перейти в корневую директорию соответствующего репозитория и выполнить команду  

Для бота:
```
$ docker image build -t nchernukha/crawler-engine:<указать версию> .
```
Для веб-интерфейса:
```
$ docker image build -t nchernukha/crawler-ui:<указать версию> .
```
4. Чтобы запушить образы в Docker Hub выполнить команды:
```
$ docker login
$ docker image push nchernukha/crawler-engine:<указать версию>
```

## Запуск приложения при помощи Docker Compose
1. Создан файл docker/docker-compose.yml с указанием всех необходимых сервисов:
	- База данных MongoDB
	- Брокер сообщений RabbitMQ
	- Crawler Search Engine
	- Crawler Search UI
2. Для запуска docker-машины в dev-окружении необходимо подключиться к нему с локального хоста:
```
$ docker-machine create \
--driver generic \
--generic-ip-address=<ip-адрес dev-хоста> \
--generic-ssh-user <имя пользователя> \
--generic-ssh-key <путь к ssh-ключу> \
<имя создаваемой docker-машины>
$ eval $(docker-machine env <имя создаваемой docker-машины>)
```
3. Для запуска приложения выполнить команды:
```
$ cd docker
$ docker-compose up -d
```
4. Приложение будет доступно по адресу `http://<имя dev-хоста>:8000/`

## Настройка мониторинга
1. Метрики приложения доступны по адресу `<внутренний IP>:8000/`
2. Создана роль Ansible
```
ansible/roles/prometheus
```
3. Создан плейбук Ansible
```
ansible/playbooks/monitoring.yml
```
4. Для установки Prometheus выполнить команды:
```
$ cd ansible
$ ansible-playbook playbooks/monitoring.yml
```
5. Метрики приложения Crawler подключены к конфигурационному файлу prometheus через переменные
```
ansible/roles/prometheus/vars/main.yml
```
## Настройка Gitlab
